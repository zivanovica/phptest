<?php

use App\Http\Middleware\ApiTokenAuthenticate;
use App\Http\Middleware\ValidateCommentOwner;
use App\Http\Middleware\ValidateCommentStatus;
use \App\Http\Middleware\ValidateThreadOwner;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/user')->group(function (): void {

    Route::post('/login', 'Auth\\LoginController@getToken');

    Route::post('/register', 'Auth\\RegisterController@registerUser');
});

Route::get('/thread/{thread_id}', 'Thread@read');
Route::get('/comment/{comment_id}', 'Comment@read');

Route::middleware(ApiTokenAuthenticate::class)->group(function (): void {

    Route::prefix('/thread')->group(function (): void {

        Route::post('/', 'Thread@create');

        Route::middleware(ValidateThreadOwner::class)->patch('/{thread_id}', 'Thread@update');

        Route::middleware(ValidateThreadOwner::class)->delete('/{thread_id}', 'Thread@delete');
    });

    Route::prefix('/comment')->group(function (): void {

        Route::post('/', 'Comment@create');

        Route::middleware(ValidateCommentStatus::class)->post('/reply/{comment_id}', 'Comment@reply');

        Route::middleware(ValidateThreadOwner::class)->patch('/status/{thread_id}/{comment_id}', 'Comment@statusUpdate');

        Route::middleware([ValidateCommentOwner::class, ValidateCommentStatus::class])->group(function (): void {

            Route::patch('/{comment_id}', 'Comment@update');

            Route::delete('/{comment_id}', 'Comment@delete');
        });
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

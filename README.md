# PHP Test #

##API Routes

NOTE: All api routes are prefixed with **/api**

Register User

**``POST /api/user/register``**
    
    Required parameters:
        - name (string)
        - email (string)
        - password (string)

Login User

**``POST /api/user/login``**
    
    Required parameters:
        - email (string)
        - password (string)
        
**Threads**

Create Thread

**``POST /thread``**

    Required Parameters:
        - api_token (string)
        - title (string)
        - content (string)
Read Thread

**``GET /thread/{id}``**

Update Thread

**``PATCH /thread/{id}``**
    
    Optional Parameters
        - title (string)
        - content (string)
        
Delete Thread

**``DELETE /thread/{id}``**

    Required Parameters:
        - api_token (string)
        
**Comments**

Create Comment

**``POST /comment``**

    Required Parameters:
        - api_token (string)
        - thread (int)
        - content (string)
        
Read Comment

**``GET /comment/{id}``**

Update Comment

**``PATCH /comment/{id}``**
    
    Required Parameters:
        - api_token (string)
    
    Optional Parameters:
        - title (stirng)
        - content (string) This is only for comment owners
        
        
Change Comment Status (Thread owners only)

**``PATCH /comment/status/{thread_id}/{comment_id}``**
    
    Required Parameters:
        - api_token (string)
        - status (string)
    
Delete Comment

**``DELETE /comment/{id}``**

    Required Parameters:
        - api_token (string) must be comment owner
        
Reply Comment

**``POST /comment/reply/{comment_id}``**
    
    Required Parameters:
        - api_token (string)
        - content (string)

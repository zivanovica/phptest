<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Comment;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table): void {

            $table->increments('id');

            $table->integer('creator_id');

            $table->integer('thread_id');

            $table->text('content');

            $table
                ->enum('status', [Comment::STATUS_PENDING, Comment::STATUS_APPROVE, Comment::STATUS_DECLINE])
                ->default(Comment::STATUS_PENDING);

            $table->integer('parent_id')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiToken extends Model
{

    protected $table = 'api_tokens';

    protected $fillable = [
        'user_id', 'token'
    ];

    public function user()
    {

        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    /**
     *
     * Creates new token for given user
     *
     * @param User $user
     * @return ApiToken|null
     */
    public static function createUserToken(User $user): ?ApiToken
    {

        $token = new ApiToken([
            'user_id' => $user->id,
            'token' => hash('sha256', uniqid('', true))
        ]);

        if ($token->save()) {

            return $token;
        }

        return null;
    }
}

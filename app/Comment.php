<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVE = 'approved';
    const STATUS_DECLINE = 'declined';

    protected $table = 'comments';

    protected $fillable = [
        'creator_id', 'thread_id', 'content', 'parent_id'
    ];

    public function thread()
    {

        return $this->hasOne(Thread::class, 'id', 'thread_id');
    }

    /**
     *
     * Creates new comment as reply on other comment
     *
     * @param Comment $comment
     * @param User $user
     * @param string $content
     * @return Comment|null
     */
    public static function reply(Comment $comment, User $user, string $content): ?Comment
    {

        if (null === $comment || false === $comment->exists()) {

            return null;
        }

        $reply = new Comment([
            'thread_id' => $comment->thread_id,
            'creator_id' => $user->id,
            'parent_id' => $comment->id,
            'content' => $content
        ]);

        if ($reply->save()) {

            return $reply;
        }

        return null;
    }

    /**
     *
     * Updates given comment status
     *
     * @param Comment $comment
     * @param string $status
     * @return Comment|null
     */
    public static function updateCommentStatus(Comment $comment, string $status): ?Comment
    {

        if (null === $comment || false === $comment->exists()) {

            return null;
        }

        $comment->status = $status;

        if ($comment->save()) {

            return $comment;
        }

        return null;
    }

    /**
     *
     * Creates new comment on given thread with given creator (user) and comment content
     *
     * @param Thread $thread
     * @param User $user
     * @param string $content
     * @return Comment|null
     */
    public static function createThreadComment(Thread $thread, User $user, string $content): ?Comment
    {

        $comment = new Comment([
            'creator_id' => $user->id,
            'thread_id' => $thread->id,
            'content' => $content,
            'status' => Comment::STATUS_PENDING
        ]);

        if ($comment->save()) {

            return $comment;
        }

        return null;
    }
}

<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{

    const THREAD_LOCK_TIME = 21600; // 6 hours

    protected $table = 'threads';

    protected $casts = [
        'creator_id' => 'integer'
    ];

    protected $fillable = [
        'title', 'content', 'creator_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {

        return $this->hasMany(Comment::class, 'thread_id', 'id');
    }

    /**
     *
     * Check wether current thread can be updated or not
     *
     * @return bool
     */
    public function canUpdate()
    {

        return (time() - strtotime($this->created_at)) <= Thread::THREAD_LOCK_TIME;
    }


    /**
     *
     * Creates new thread and returns instance of its model
     *
     * @param User $creator
     * @param string $title
     * @param string $content
     * @return Thread|null
     */
    public static function createNew(User $creator, string $title, string $content): ?Thread
    {
        $thread = new \App\Thread([
            'creator_id' => $creator->id,
            'title' => $title,
            'content' => $content
        ]);

        if ($thread->save()) {

            return $thread;
        }

        return null;
    }

    /**
     *
     * Updated existing thread with given id and provided content data (title, content)
     *
     * @param int $threadId
     * @param array $threadData
     * @return Thread|null
     */
    public static function updateThreadContents(int $threadId, array $threadData): ?Thread
    {

        /** @var Thread $thread */
        $thread = Thread::find($threadId);

        if (null === $thread || false === $thread) {

            return null;
        }

        if (false === $thread->canUpdate()) {

            return null;
        }

        $thread->fill($threadData);

        if ($thread->save()) {

            return $thread;
        }

        return null;
    }

    /**
     *
     * Deletes given thread id from database, return TRUE on success and FALSE on failure
     *
     * @param int $threadId
     * @return bool
     */
    public static function deleteThread(int $threadId): bool
    {

        $thread = Thread::find($threadId);

        if (null === $thread || false === $thread->exists()) {

            return false;
        }

        if ($thread->delete()) {

            return true;
        }

        return false;
    }
}

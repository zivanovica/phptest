<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Thread extends Controller
{

    /**
     *
     * Retrieve information about certain thread
     *
     * @param Response $response
     * @param int $id
     * @return Response
     */
    public function read(Response $response, int $id): Response
    {

        $thread = \App\Thread::find($id);

        if (null == $thread) {

            $response->setStatusCode(404)->setContent(['message' => 'Thread was not found.']);
        } else {

            $response->setStatusCode(200)->setContent($thread);
        }

        return $response;
    }

    /**
     *
     * Create new thread
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function create(Request $request, Response $response): Response
    {

        $data = $this->validate($request, [
            'title' => 'required|min:2|max:64',
            'content' => 'required|min:20|max:400'
        ]);

        $thread = \App\Thread::createNew($request->user(), $data['title'], $data['content']);

        if (null === $thread) {

            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)->setContent([
                'message' => 'Failed to create thread.'
            ]);
        }

        return $response->setContent($thread);
    }

    /**
     *
     * Update already existing thread
     *
     * @param Request $request
     * @param Response $response
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Response $response, int $id): Response
    {

        $data = $request->validate([
            'title' => 'min:2|max:64',
            'content' => 'min:20|max:400'
        ]);

        $thread = \App\Thread::updateThreadContents($id, $data);

        if (null === $thread) {

            return $response
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent(['message' => 'Failed to update thread contents']);
        }

        return $response->setContent($thread);
    }

    /**
     *
     * Deletes given thread
     *
     * @param Request $request
     * @param Response $response
     * @param int $id
     * @return Response
     */
    public function delete(Request $request, Response $response, int $id): Response
    {

        if (false === \App\Thread::deleteThread($id)) {

            return $response
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent(['message' => 'Failed to remove thread']);
        }

        return $response->setContent(['message' => 'Thread successfully removed']);
    }
}
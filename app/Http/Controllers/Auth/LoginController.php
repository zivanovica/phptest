<?php

namespace App\Http\Controllers\Auth;

use App\ApiToken;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     *
     * Creates and returns token for given user using email and password
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getToken(Request $request, Response $response): Response
    {

        $data = $request->validate([
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ]);

        /** @var User $user */
        $user = User::where(['email' => $data['email']])->first();

        if (null == $user) {

            return $response->setStatusCode(404)->setContent(['message' => 'Invalid email.']);
        }

        if (false === password_verify($data['password'], $user->password)) {

            return $response
                ->setStatusCode(Response::HTTP_UNAUTHORIZED)->setContent(['message' => 'Invalid credentials']);
        }

        $apiToken = ApiToken::createUserToken($user);

        if (null === $apiToken) {

            return $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)->setContent(['message' => 'Error while creating token']);
        }

        return $response->setContent($apiToken);
    }
}

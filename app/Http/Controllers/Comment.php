<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Comment extends Controller
{

    /**
     *
     * Retrieve wanted comment using comment id
     *
     * @param Response $response
     * @param int $commentId
     * @return Response
     */
    public function read(Response $response, int $commentId): Response
    {

        $comment = \App\Comment::find($commentId);

        if (null === $comment || false === $comment->exists()) {

            return $response->setStatusCode(Response::HTTP_NOT_FOUND)->setContent([
                'message' => 'Comment not found'
            ]);
        }

        return $response->setContent($comment);
    }

    /**
     *
     * Creates new comment for given thread
     *
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function create(Request $request, Response $response): Response
    {

        $data = $request->validate([
            'thread_id' => 'required|exists:threads,id',
            'content' => 'required|min:2|max:400'
        ]);

        $comment = \App\Comment::createThreadComment(
            \App\Thread::find($data['thread_id']),
            $request->user(),
            $data['content']
        );

        if (null === $comment) {

            return $response
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent(['message' => 'Failed to create comment']);
        }

        return $response->setContent($comment);
    }

    /**
     * Updates existing comment
     *
     * NOTE: this won't affect comment status nor thread_id just comment content
     *
     * @param Request $request
     * @param Response $response
     * @param int $commentId
     * @return Response
     */
    public function update(Request $request, Response $response, int $commentId): Response
    {

        $data = $request->validate([
            'content' => 'min:2|max:400'
        ]);

        $comment = \App\Comment::find($commentId);

        if (null === $comment || false === $comment->exists()) {

            return $response
                ->setStatusCode(Response::HTTP_NOT_FOUND)
                ->setContent(['message' => 'Invalid comment']);
        }

        $comment->fill($data);

        if ($comment->save()) {

            return $response->setContent(['message' => 'Comment successfully updated']);
        }

        return $response
            ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
            ->setContent(['message' => 'Failed to update comment']);
    }

    /**
     *
     * Removes given comment
     *
     * @param Response $response
     * @param int $commentId
     * @return Response
     */
    public function delete(Response $response, int $commentId): Response
    {

        $comment = \App\Comment::find($commentId);

        if (null === $comment || false === $comment->exists()) {

            return $response
                ->setStatusCode(Response::HTTP_NOT_FOUND)
                ->setContent(['message' => 'Invalid comment']);
        }

        if ($comment->delete()) {

            return $response->setContent(['message' => 'Comment successfully removed']);
        }

        return $response
            ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
            ->setContent(['message' => 'Failed to remove comment']);

    }

    /**
     *
     * Update comment status (e.g from pending to approved), this is controlled only by thread owner
     *
     * @param Request $request
     * @param Response $response
     * @param int $threadId
     * @param int $commentId
     * @return Response
     */
    public function statusUpdate(Request $request, Response $response, int $threadId, int $commentId): Response
    {

        $acceptableStatuses = implode(',', [
            \App\Comment::STATUS_PENDING, \App\Comment::STATUS_APPROVE, \App\Comment::STATUS_DECLINE]
        );

        $data = $request->validate([
            'status' => "required|in:{$acceptableStatuses}"
        ]);

        $comment = \App\Comment::updateCommentStatus(\App\Comment::find($commentId), $data['status']);

        if (null === $comment) {

            return $response->setContent(['message' => 'Failed to update comment status']);
        }

        return $response->setContent(['message' => 'Comment status successfully updated.']);
    }

    /**
     *
     * Reply on comment with comment
     *
     * @param Request $request
     * @param Response $response
     * @param int $commentId
     * @return Response
     */
    public function reply(Request $request, Response $response, int $commentId): Response
    {

        $data = $request->validate([
            'content' => 'required|min:2|max:400'
        ]);

        $comment = \App\Comment::reply(\App\Comment::find($commentId), $request->user(), $data['content']);

        if (null === $comment) {

            return $response
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
                ->setContent(['message' => 'Failed to reply']);
        }

        return $response->setContent($comment);
    }

}
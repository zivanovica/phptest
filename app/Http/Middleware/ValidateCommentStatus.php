<?php

namespace App\Http\Middleware;

use App\Comment;
use Closure;
use Illuminate\Http\Response;

class ValidateCommentStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $comment = Comment::find($request->route('comment_id'));

        if (null === $comment || false === $comment->exists()) {

            return \response(['message' => 'Invalid comment'], Response::HTTP_NOT_FOUND);
        }

        if (Comment::STATUS_APPROVE === $comment->status) {

            return $next($request);
        }

        return \response(['message' => 'Comment is not approved'], Response::HTTP_NOT_ACCEPTABLE);
    }
}

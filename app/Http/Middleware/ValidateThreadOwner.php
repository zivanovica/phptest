<?php

namespace App\Http\Middleware;

use App\Thread;
use Closure;
use Illuminate\Http\Response;

class ValidateThreadOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();

        if (null === $user || false === $user->exists()) {

            return response(['message' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }

        $thread = Thread::where('creator_id', $user->id)
            ->where('id', $request->route('thread_id'))->first();

        if (null === $thread || false === $thread->exists()) {

            return \response(['message' => 'Invalid thread owner'], Response::HTTP_NOT_FOUND);
        }

        return $next($request);
    }
}

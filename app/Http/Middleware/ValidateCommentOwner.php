<?php

namespace App\Http\Middleware;

use App\Comment;
use Closure;
use Illuminate\Http\Response;

class ValidateCommentOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();

        if (null === $user || false === $user->exists()) {

            return response(['message' => 'Invalid user'], Response::HTTP_BAD_REQUEST);
        }

        $comment = Comment::where('creator_id', $user->id)
            ->where('id', $request->route('comment_id'))->first();

        if (null === $comment || false === $comment->exists()) {

            return \response(['message' => 'Invalid comment owner'], Response::HTTP_NOT_FOUND);
        }

        return $next($request);
    }
}

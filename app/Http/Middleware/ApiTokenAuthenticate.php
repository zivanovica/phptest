<?php

namespace App\Http\Middleware;

use App\ApiToken;
use Closure;
use Illuminate\Http\Response;

class ApiTokenAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $data = $request->validate([
            'api_token' => 'required|exists:api_tokens,token'
        ]);

        $apiToken = ApiToken::where('token', $data['api_token'])->first();

        if ($apiToken->user && $apiToken->user->exists()) {

            $request->setUserResolver(function () use ($apiToken): \App\User {

                return $apiToken->user;
            });

            return $next($request);
        }

        return response(['message' => 'Invalid user'], Response::HTTP_NOT_FOUND);
    }
}
